from django.urls import path
from . import views

urlpatterns = [
    path('', views.all, name="blog"),
    path('<int:post_id>/', views.detail, name="detail")
]
