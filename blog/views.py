from django.shortcuts import render, get_object_or_404
from .models import BlogPost


def all(request):
    posts = BlogPost.objects

    return render(request, "blog/all.html", {"latest": posts})


def detail(request, post_id):
    post = get_object_or_404(BlogPost, pk=post_id)

    return render(request, "blog/detail.html", {"post": post})
