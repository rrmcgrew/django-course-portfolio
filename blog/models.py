from django.db import models


class BlogPost(models.Model):
    title = models.CharField(max_length=255)
    pub_date = models.DateTimeField()
    body = models.TextField()
    image = models.ImageField(upload_to="images/")

    def __str__(self):
        return self.title

    def summary(self):
        """provides an abbreviated view of the post body if it is longer than
        100 characters
        """
        if len(self.body) > 100:
            summary = self.body[:94] + " . . ."
        else:
            summary = self.body

        return summary

    def pub_date_pretty(self):
        """creates a more reading-friendly format for the pub_date"""
        return self.pub_date.strftime("%b %e, %Y")
