To launch virtual environment:
`source /path/to/virt/bin/activate`

To stop virtual environment:
`deactivate`
